# Organization Variables
variable "create_org" {
  type        = bool
  default     = false
  description = "This variable controls if Organization will be created or not"
}

variable "aws_service_access_principals" {
  type        = list(any)
  default     = []
  description = "List of AWS service principal names for which you want to enable integration with your organization"
}

variable "enabled_policy_types" {
  type        = list(any)
  default     = []
  description = "List of Organizations Policy Types to enable in the Organization Root"
}

variable "feature_set" {
  type        = string
  default     = "ALL"
  description = "Specify 'ALL' (default) or 'CONSOLIDATED_BILLING'"
}

# Organizational Units Variables
variable "create_organizational_units" {
  type        = bool
  default     = false
  description = "This variable controls if Organizations Organizational unit will be created or not"
}

variable "organizational_units" {
  type = list(object({
    name        = string
    parent_name = string
  }))
  default     = []
  description = "Organizational Units"
}

# Organizations Accounts
variable "create_organizations_accounts" {
  type        = bool
  default     = false
  description = "This variable controls if Organizations Accounts will be created or not"
}

variable "organizations_accounts" {
  type = list(object({
    name        = string
    email       = string
    parent_name = string
    role_name   = string
  }))
  default     = []
  description = "Organizations Accounts"
}

# Organizations Policy
variable "create_organizations_policies" {
  type        = bool
  default     = false
  description = "This variable controls if Organizations Policies will be created or not"
}

variable "organizations_policy" {
  type = list(object({
    name        = string
    description = string
    type        = string
    content     = string
  }))
  default     = []
  description = "Organizations Policies"
}


variable "policy_id" {
  default     = ""
  description = "The unique identifier (ID) of the policy that you want to attach to the target"
}

variable "target_id" {
  type        = any
  default     = []
  description = "The unique identifier (ID) of the root, organizational unit, or account number that you want to attach the policy to"
}

variable "tags" {
  type        = map(string)
  description = "Tags"
  default     = {}
}