/*
 * # Aws organizations
 *
 * ## Usage
 * 
 * ```hcl-terraform
 * provider "aws" {}
 * 
 * module "aws-s3-static-website-cloudfront-with-ovh-dns" {
 *   source                         = "git::https://gitlab.com/keltiotechnology/terraform/infras/aws-keltio-organization"
 *  
 *   create_org                     = true
 *
 *   create_organizational_units    = true
 *
 *   organizational_units           = [
 *   {
 *    name      = "Sandbox",
 *    parent_name = ""
 *   },
 *   {
 *    name      = "Workloads",
 *    parent_name = ""
 *   },
 *   {
 *    name      = "Prod",
 *    parent_name = "Workloads"
 *   }
 *  ]
 *
 *   create_organizations_accounts  = true
 * 
 *   organizations_accounts         = [
 *   {
 *    name      = "account_name",
 *    email     = "account@email.com",
 *    parent_name = "Sandbox", (doesn't work with nested OUs)
 *    role_name = ""
 *   },
 *  ]
 * ```
 *
*/


locals {
  policy_id = coalescelist(aws_organizations_policy.policy.*, [var.policy_id])
}

################
# Organization #
################
resource "aws_organizations_organization" "organization" {

  count = var.create_org ? 1 : 0

  aws_service_access_principals = var.aws_service_access_principals
  enabled_policy_types          = var.enabled_policy_types
  feature_set                   = var.feature_set
}

data "aws_organizations_organization" "organization" {}

########################
# Organizational Units #
########################
resource "aws_organizations_organizational_unit" "root_ous" {

  for_each = var.create_organizational_units ? { for ou in var.organizational_units : ou.name => ou if ou.parent_name == "" } : {}

  name      = each.value.name
  parent_id = data.aws_organizations_organization.organization.roots[0].id
}

data "aws_organizations_organizational_units" "ou" {
  depends_on = [
    aws_organizations_organizational_unit.root_ous
  ]
  parent_id = data.aws_organizations_organization.organization.roots[0].id
}

resource "aws_organizations_organizational_unit" "nested_ous" {
  depends_on = [
    data.aws_organizations_organizational_units.ou
  ]
  for_each = var.create_organizational_units ? { for ou in var.organizational_units : ou.name => ou if length(ou.parent_name) > 0 } : {}

  name      = each.value.name
  parent_id = join("", [for ou in data.aws_organizations_organizational_units.ou.children : ou.id if ou.name == each.value.parent_name])
}

############################
#  Organizations Accounts  #
############################
data "aws_organizations_organizational_units" "all_ous" {
  depends_on = [
    aws_organizations_organizational_unit.nested_ous
  ]
  parent_id = data.aws_organizations_organization.organization.roots[0].id
}

resource "aws_organizations_account" "accounts" {
  depends_on = [
    data.aws_organizations_organizational_units.all_ous
  ]
  for_each = var.create_organizations_accounts ? { for acc in var.organizations_accounts : acc.name => acc } : {}

  name      = each.value.name
  email     = each.value.email
  parent_id = length(each.value.parent_name) > 0 ? join("", [for ou in data.aws_organizations_organizational_units.all_ous.children : ou.id if ou.name == each.value.parent_name]) : data.aws_organizations_organization.organization.roots[0].id
  role_name = length(each.value.role_name) > 0 ? each.value.role_name : null

  tags = var.tags
}

##########################
#  Organizations Policy  #
##########################
resource "aws_organizations_policy" "policy" {
  for_each = var.create_organizations_policies ? { for poli in var.organizations_policy : poli.name => poli } : {}

  name        = each.value.name
  description = each.value.description

  type = each.value.type

  content = each.value.content

  tags = var.tags
}

resource "aws_organizations_policy_attachment" "policy_attachment" {
  count = length(var.target_id) > 0 ? length(var.target_id) : 0

  policy_id = local.policy_id[0]
  target_id = tolist(var.target_id)[count.index]
}
